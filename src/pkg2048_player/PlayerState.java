/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg2048_player;

/**
 * state enumeration to describe current state of AI
 * OBSERVING; scanning board and decoding colors
 * CALCULATING; deciding on next move
 * FINISHED; no moves were found
 * @author joris
 */
public enum PlayerState {
    OBSERVING,
    CALCULATING,
    FINISHED
}
