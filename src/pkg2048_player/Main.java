/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg2048_player;

import ai.GameTreeStrategy;
import gui.JStatusFrame;
import java.awt.AWTException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author joris
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, AWTException {

        // load icon 
        BufferedImage img = null;       
        try {
             img = ImageIO.read(Main.class.getResourceAsStream("/gui/Apps-Evernote-Metro-icon-48.png"));
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // create player
        final Player p = new Player();
        p.setLogging(false);
        p.setInterDelay(400);
        p.setStrategy(new GameTreeStrategy());
        
        // information frame
        final JStatusFrame f = new JStatusFrame(p);
        f.setIconImage(img);
        f.setTitle("heracles");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();
        f.setResizable(false);
        f.setVisible(true);
                
        p.addListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                f.setState(p.getState());
                f.setBoard(p.getBoard());
                f.setMetrics(p.getBoard());
            }
        });
        
    }
    
}
