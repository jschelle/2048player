/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg2048_player;

import board.Direction;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author joris
 */
public class Player extends ChangeListenerModel{
    
    // related to physical typing
    private long inter_delay = 1000;
    private long startup_delay = 10000;
    private Robot robot = null;
    
    // model of board
    int[][] m_board = null;
    int[][] m_prev_board = null;
    
    // strategy
    private Strategy m_strategy = null;
    private PlayerState m_state = PlayerState.OBSERVING;
    
    // debug
    private boolean m_restart;
    private long m_start_of_game = 0;
    private boolean m_logging = false;
    private int m_log_count = 0;
    
    public Player(){
        try {
            robot = new Robot();
        } catch (AWTException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean shouldStop(){
        boolean unknownSquare = false;
        for(int i=0;i<m_board.length;i++){
            for(int j=0;j<m_board.length;j++){
                if(m_board[i][j]==-1){
                    unknownSquare = true;
                    break;
                }
            }
            if(unknownSquare)
                break;
        }  
        return unknownSquare;
    }
    private void safeSleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public PlayerState getState(){
        return m_state;
    }
        
    public void setStrategy(Strategy s){
        m_strategy = s;
    }        
    public void setRestart(boolean aFlag){
        m_restart = aFlag;
    }
    public void setStartupDelay(long ms){
        startup_delay = java.lang.Math.max(ms,5000);        
    }
    public void setInterDelay(long ms){
        inter_delay = java.lang.Math.max(ms,200);
    }    
    public void setLogging(boolean aFlag){
        m_logging = aFlag;
    }
        
    public void start(){
        new Thread(){
            @Override
            public void run(){
                playGame();
            }
        }.start();
    }
    
    public void preGame(){        
    }
    public void playGame(){
        
        // reset
        m_strategy.reset();
        preGame();
        
        // change state    
        m_state = PlayerState.OBSERVING;        
        fireStateChanged();
        
        // get everything ready for logging
        m_start_of_game = System.currentTimeMillis();       
        m_log_count = 0;        
        safeSleep(startup_delay);
        
        // scan board 
        m_board = Scanner.scan();        
        fireStateChanged();
        
        while(!shouldStop() && !m_strategy.isDone()){
 
            if(m_logging){
                createLogEntry();
            }
            
            // set previous board
            m_prev_board = m_board;
            
            // calculate solution
            m_state = PlayerState.CALCULATING;
            fireStateChanged();
            Direction dir = m_strategy.decide(m_board);
            pressKey(dir);
            
            // observe board
            m_state = PlayerState.OBSERVING;  
            fireStateChanged();
            safeSleep(inter_delay);           
            m_board = Scanner.scan();
            
        }
        
        // mark finished
        m_state = PlayerState.FINISHED;
        fireStateChanged();
        
        // debug
        if(m_restart){
            postGame();
            restartGame();
            playGame();
        }
        
    }  
    public void postGame(){        
    }
    
    public int[][] getBoard(){
        if(m_state==PlayerState.FINISHED)
            return m_prev_board;
        return m_board;
    }
   
    private void createLogEntry(){
        // create base directory for logging
        File home_dir = new File(System.getProperty("user.home"));
        File program_dir = new File(home_dir,"2048_player");
        if(!program_dir.exists())
            program_dir.mkdir();
        
        //  create subdirectory for logging
        SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy@hh:mm:ss");
        String log_name = format.format(new Date(m_start_of_game));
        File log_dir = new File(program_dir,log_name);
        if(!log_dir.exists())
            log_dir.mkdir();
        
        // create screencapture
        Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        BufferedImage capture = robot.createScreenCapture(screenRect);  
        
        // write screencapture
        String log_filename = m_log_count + ".png";
        try {
            ImageIO.write(capture, "png", new File(log_dir,log_filename));
        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
        m_log_count++;
    }
        
    private void pressKey(Direction d){        
        if(d==Direction.UP){
            robot.keyPress(KeyEvent.VK_UP);  
            robot.keyRelease(KeyEvent.VK_UP);
        }
        else if(d==Direction.RIGHT){
            robot.keyPress(KeyEvent.VK_RIGHT);
            robot.keyRelease(KeyEvent.VK_RIGHT);                
        }
        else if(d==Direction.DOWN){
            robot.keyPress(KeyEvent.VK_DOWN);
            robot.keyRelease(KeyEvent.VK_DOWN);
        }
        else if(d==Direction.LEFT){
            robot.keyPress(KeyEvent.VK_LEFT);
            robot.keyRelease(KeyEvent.VK_LEFT);
        }        
    }
    private void restartGame(){
        robot.keyPress(KeyEvent.VK_F5);
        robot.keyRelease(KeyEvent.VK_F5);
    }
    
}
