/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg2048_player;

import ai.NodeScore;
import board.Metrics;
import java.util.Arrays;

/**
 *
 * @author joris
 */
public class HeuristicPlayer extends Player{
   
    private final java.util.Random rnd = new java.util.Random();
    
    // average results for current parameters
    private double m_val_avg = 0;
    private double m_prev_val_avg = 0;
    private double[] m_prev_weights = new double[5];
    
    // constants used for averaging out results
    private final int m_max_games = 5;
    private int m_game_count = 0;
    
    public HeuristicPlayer(){
        super.setRestart(true);
        super.setLogging(true);
        NodeScore.setWeights(new double[]{600,250,500,400,510});
    }    
   
    @Override
    public void postGame(){
        m_game_count++;
        m_val_avg+=Metrics.highestValue(getBoard())*2048;
        if(m_game_count>=m_max_games){
            // reset
            m_game_count = 0;
            // calc average
            m_val_avg/=m_max_games;
            // call optimization
            nextIteration();
        }
    }
    
    private void nextIteration(){
             
        // some logging
        double[] w = NodeScore.getWeights();
        double[] step = {-100,-10,-1,1,10,100};
        
        // printout
        System.out.println(Arrays.toString(w) + "\t" + m_val_avg);
        
        // revert to previous if score declines
        if(m_val_avg < m_prev_val_avg){
            NodeScore.setWeights(m_prev_weights);
            return;
        }
        
        // store current (as previous)
        m_prev_val_avg = m_val_avg;
        m_prev_weights = Arrays.copyOf(w,w.length);
        
        // find neighbour
        while(Arrays.equals(w, m_prev_weights)){         
            w[rnd.nextInt(w.length)]+=step[rnd.nextInt(step.length)];
        }
              
        // adjust
        NodeScore.setWeights(w);
    }


}
