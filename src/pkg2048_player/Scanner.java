/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg2048_player;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joris
 */
public class Scanner {
   
    private static final Map<Color,Integer> m_colors = new HashMap<>();
    private static final Color m_border_color = Color.decode("#beb09e");
    private static final int m_border_thickness = 14;
    private static final int m_square_size = 107;
    
    /**
     * load presets of this scanner
     * (i.e. what color is associated with what number)
     */
    private static void loadPresets(){        
        java.util.Scanner sc = new java.util.Scanner(Scanner.class.getResourceAsStream("/pkg2048_player/scanner_presets"));
        while(sc.hasNextLine()){
            String[] line = sc.nextLine().split(" ");
            String colorString = line[0];
            String valueString = line[line.length-1];
            Color c = Color.decode("#"+colorString);
            int v = Integer.parseInt(valueString);
            m_colors.put(c, v);
        }        
    }
    
    /**
     * scan the current screen to find an open instance of the game
     * @return 
     */
    public static int[][] scan(){
        loadPresets();
        int[][] board = new int[4][4];
        try {
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage capture = new Robot().createScreenCapture(screenRect);          
            // search vertical edge            
            int vpos = -1;
            for(int c=0;c<capture.getWidth();c++){
                if(isVerticalStartOfGrid(capture,c)){
                    vpos = c;
                    break;
                }
            }
            // search horizontal edge
            int hpos = -1;
            for(int r=0;r<capture.getHeight();r++){
                if(isHorizontalStartOfGrid(capture,r)){
                    hpos = r;
                    break;
                }
            }            
            // find pixel values           
            for(int i=0;i<4;i++){
                for(int j=0;j<4;j++){
                    int x = vpos + m_border_thickness*(i+1) + i*m_square_size + (m_square_size/2);
                    int y = hpos + m_border_thickness*(j+1) + j*m_square_size + (m_square_size/2) ;
                    int v = decodePosition(x, y, capture);
                    board[j][i]=v;
                }
            }
        } catch (AWTException ex) {
            Logger.getLogger(Scanner.class.getName()).log(Level.SEVERE, null, ex);
        }
        // default
        return board;
    }
    
    /**
     * check whether a given vertical line in the capture image is the vertical 
     * topmost-border of the game
     * @param capture
     * @param col
     * @return 
     */
    private static boolean isVerticalStartOfGrid(BufferedImage capture, int col){
        int count = 0;
        for(int i=0;i<capture.getHeight();i++){
            Color c = new Color(capture.getRGB(col, i));
            if(distance(c,m_border_color)<5)
                count++;
        }
        if(count>m_square_size)
            return true;
        // default
        return false;
    }
    
    /**
     * check whether a given horizontal line in the capture image is the horizontal 
     * leftmost-border of the game
     * @param capture
     * @param row
     * @return 
     */
    private static boolean isHorizontalStartOfGrid(BufferedImage capture, int row){
        int count = 0;
        for(int i=0;i<capture.getWidth();i++){
            Color c = new Color(capture.getRGB(i,row));
            if(distance(c,m_border_color)<5)
                count++;
        }
        if(count>m_square_size)
            return true;
        // default
        return false;
    }    
    
    /**
     * decide what number should be associated with a given position
     * tries out 8 positions centered around the given coordinates
     * @param x
     * @param y
     * @param capture
     * @return 
     */
    private static int decodePosition(int x, int y, BufferedImage capture){
        Graphics g = capture.getGraphics();
        int[] offset = {-25,25};
        for(int xoff : offset){
            for(int yoff : offset){
                int xn = x+xoff;
                int yn = y+yoff;
                if(xn<capture.getWidth() && xn>=0 && yn<capture.getHeight() && yn>=0){
                    int v = decodeColor(capture.getRGB(xn, yn));
                    if(v!=-1)
                        return v;
                }
            }
        }
        return -1;
    }
    
    /**
     * translates a color to an int-value
     * @param rgb
     * @return 
     */
    private static int decodeColor(int rgb){  
        Color c = new Color(rgb);
        for(Entry<Color,Integer> en : m_colors.entrySet()){
            if(distance(en.getKey(),c)<5)
                return en.getValue();
        }
        return -1;
    }
    private static double distance(Color c0, Color c1){
        double r = c0.getRed()-c1.getRed();
        double b = c0.getBlue()-c1.getBlue();
        double g = c0.getGreen()-c1.getGreen();
        return java.lang.Math.sqrt(r*r+b*b+g*g);
    }
    
}
