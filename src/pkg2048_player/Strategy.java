/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg2048_player;

import board.Direction;

/**
 *
 * @author joris
 */
public interface Strategy {
    
    /**
     * decide on a direction for the current game
     * @param board
     * @return 
     */
    public Direction decide(int[][] board);
    
    /**
     * return whether a strategy is finished with the current game
     * @return 
     */
    public boolean isDone();
    
    /**
     * reset the strategy
     * so that it is ready for another game
     */
    public void reset();
    
}
