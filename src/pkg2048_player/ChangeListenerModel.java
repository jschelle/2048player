/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg2048_player;

import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author joris
 */
public class ChangeListenerModel {
    
    private List<ChangeListener> listeners = new ArrayList<>();
    
    public void addListener(ChangeListener l){
        listeners.add(l);
    }
    public void removeListener(ChangeListener l){
        listeners.remove(l);
    }
    public void fireStateChanged(){
        ChangeEvent evt = new ChangeEvent(this);
        for(int i=listeners.size()-1;i>=0;i--){
            listeners.get(i).stateChanged(evt);
        }
    }
}
