/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cache;

import ai.AlphaBetaTreeNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author joris
 */
public class Cache {
    
    private final int cacheLimit = 10000;
    private final Map<String,List<AlphaBetaTreeNode>> cache = new HashMap<>();
    
    public void update(String hash, List<AlphaBetaTreeNode> l){
        if(cache.size()>cacheLimit){
            cleanupCache();
        }
        cache.put(hash, l);
    }
    private void cleanupCache(){
        List<String> keys = new ArrayList<>(cache.keySet());
        java.util.Collections.shuffle(keys);
        for(int i=0;i<500;i++){
            cache.remove(keys.get(i));
        }
    }
    
    public boolean contains(String hash){
        return cache.containsKey(hash);
    }
    public List<AlphaBetaTreeNode> fetch(String hash){
        if(cache.containsKey(hash))
            return cache.get(hash);
        return null;
    }
}
