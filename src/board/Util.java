/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package board;

/**
 *
 * @author joris
 */
public class Util {
    
    /**
     * test whether two given boards are equal to one another
     * @param b0
     * @param b1
     * @return 
     */
    public static boolean equals(int[][] b0, int[][] b1){
        for(int i=0;i<b0.length;i++){
            for(int j=0;j<b0.length;j++){
                if(b0[i][j]!=b1[i][j])
                    return false;
            }
        }
        // default
        return true;
    }
    
    /**
     * create a deep copy of a board
     * @param b
     * @return 
     */
    public static int[][] deepCopy(int[][] b){
        int[][] v = new int[b.length][b.length];
        for(int i=0;i<b.length;i++)
            for(int j=0;j<b.length;j++)
                v[i][j] = b[i][j];
        return v;
    }
    
    /**
     * create a textual representation of a board (represented by int[][])
     * @param b
     * @return 
     */
    public static String toString(int[][] b){
        String s = "";
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                if(b[i][j]==-1)
                    s+="□";
                else
                    s+=(b[i][j]+" ");
            }
            s+="\n";
        }        
        return s;
    }    
        
}
