/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package board;

/**
 *
 * @author joris
 */
public class Motion {
    
    /**
     * apply upwards gravity to the board
     * @param b
     * @return 
     */
    public static int[][] up(int[][] b){
       int i,j,k;
	int[][] ret = new int[4][4];

	for(i=0;i<4;i++)		// column index
	{
		k = 0;
		for(j=0;j<4;j++)	// row index
		{
			// skip empty fields (i.e. apply gravity)
			if(b[j][i]!=0)
			{
				if(j!=0 && k!=0 && b[j][i]==ret[k-1][i])	// if field matches field above it, merge
				{
					ret[k-1][i]*=-2;
				}else{
					ret[k][i] = b[j][i];                    // otherwise copy field
					k++;
				}
			}
		}
	}
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			ret[i][j] = java.lang.Math.abs(ret[i][j]);
		}
	}

	return ret;
    } 
    
    /**
     * apply downwards gravity
     * @param b
     * @return 
     */
    public static int[][] down(int[][] b){
        int i,j,k;
	int[][]  ret = new int[4][4];
	for(i=0;i<4;i++)		// column index
	{
		k = 3;
		for(j=3;j>=0;j--)	// row index
		{
			// skip empty fields (i.e. apply gravity)
			if(b[j][i]!=0)
			{
				if(j!=3 && k!=3 && b[j][i]==ret[k+1][i])	// if field matches field below it, merge
				{
					ret[k+1][i]*=-2;
				}else{
					ret[k][i] = b[j][i];                    // otherwise copy field
					k--;
				}
			}
		}
	}
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			ret[i][j] = java.lang.Math.abs(ret[i][j]);
		}
	}

	return ret;	
    }
    
    /**
     * apply leftwards gravity
     * @param b
     * @return 
     */
    public static int[][] left(int[][] b){
        int i,j,k;
	int[][] ret = new int[4][4];
	for(i=0;i<4;i++)		// row index
	{
		k = 0;
		for(j=0;j<4;j++)	// col index
		{
			// skip empty fields (i.e. apply gravity)
			if(b[i][j]!=0)
			{
				if(j!=0 && k!=0 && b[i][j]==ret[i][k-1])	// if field matches field above it, merge
				{
					ret[i][k-1]*=-2;
				}else{
					ret[i][k] = b[i][j];                    // otherwise copy field
					k++;
				}
			}
		}
	}
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			ret[i][j] = java.lang.Math.abs(ret[i][j]);
		}
	}
	return ret;
    }
    
    /**
     * apply rightwards gravity
     * @param b
     * @return 
     */
    public static int[][] right(int[][] b){
        int i,j,k;
	int[][] ret = new int[4][4];
	for(i=0;i<4;i++)		// row index
	{
		k = 3;
		for(j=3;j>=0;j--)	// col index
		{
			// skip empty fields (i.e. apply gravity)
			if(b[i][j]!=0)
			{
				if(j!=3 && k!=3 && b[i][j]==ret[i][k+1])	// if field matches field next to it, merge
				{
					ret[i][k+1]*=-2;
				}else{
					ret[i][k] = b[i][j];                    // otherwise copy field
					k--;
				}
			}
		}
	}
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			ret[i][j] = java.lang.Math.abs(ret[i][j]);
		}
	}

	return ret;
    }
                 
}
