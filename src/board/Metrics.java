/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package board;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author joris
 */
public class Metrics {
   

    public static double gameOverMetric(int[][] b){
        int[][] u = Motion.up(b);
        int[][] r = Motion.right(b);
        int[][] d = Motion.down(b);
        int[][] l = Motion.left(b);
        // reward 2048
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)
                if(b[i][j]==2048)
                    return 1;
        // punish end-of-game leaves
        if(Util.equals(b, u) && Util.equals(b, r) && Util.equals(b, d) && Util.equals(b, l))
            return -1;
        // neutral for inter-tree nodes
        return 0;
    }
    
    public static double bottomMonotone(int[][] b){
        double a=0,d=0,e=0;
        for(int i=0;i<3;i++)
            if(b[3][i]>b[3][i+1])
                a++;
            else if(b[3][i]<b[3][i+1])
                d++;
            else
                e++;
        return (java.lang.Math.max(a,d)+e)/3.0;
    }
    public static double bottomHeavy(int[][] b){
        // put elements in list
        List<Integer> l = new ArrayList<>();
        for (int[] b1 : b) {
            for (int j = 0; j<b.length; j++) {
                l.add(b1[j]);
            }
        }
        java.util.Collections.sort(l,java.util.Collections.reverseOrder()); 
        // calculate sum of 5 heaviest elements
        double sum0 = 0;
        for(int i=0;i<b.length;i++)
            sum0+=l.get(i);
        // calculate sum of bottom row
        double sum1 = 0;
        for(int i=0;i<b.length;i++)
            sum1+=b[b.length-1][i];
        // calculate ratio
        return sum1/sum0;
    }
    public static double emptySquares(int[][] b){
        int count = 0;
        for(int i=0;i<b.length;i++)
            for(int j=0;j<b.length;j++)
                if(b[i][j]==0)
                    count++;
        return count/16.0;
    }
    
    public static double highestValue(int[][] b){
        int max = 0;
        for(int i=0;i<b.length;i++)
            for(int j=0;j<b.length;j++)
                if(b[i][j]>max)
                    max=b[i][j];
        return max/2048.0;        
    }
    
    public static double avgValue(int[][] b){
        double avg = 0;
        double count = 0;
        for(int i=0;i<b.length;i++)
            for(int j=0;j<b.length;j++){
                if(b[i][j]!=0){
                    avg+=b[i][j];
                    count++;
                }
            }
        if(count==0)
            return 0;
        return avg/count;         
    }
    public static double deviance(int[][] b){
        double avg = avgValue(b);
        double dev = 0;
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)
                dev+=java.lang.Math.abs(b[i][j]-avg);
        return dev;
    }
}
