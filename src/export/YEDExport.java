/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package export;

import ai.AlphaBetaTreeNode;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 *
 * @author joris
 */
public class YEDExport {
    
    private static Map<String,Integer> names = new HashMap<>();
    
    public static String export(AlphaBetaTreeNode root){
        String retval = "";
        // handle nodes        
        names.clear();
        Stack<AlphaBetaTreeNode> stk = new Stack<>();
        stk.push(root);
        while(!stk.isEmpty()){
            AlphaBetaTreeNode n = stk.pop();
            retval+=exportNode(n);
            
        }
        // return
        return retval;
    }
    private static String exportNode(AlphaBetaTreeNode n){
        // determine name
        String nodename = "";
        if(names.containsKey(n.hash))
            nodename =  "n"+names.get(n.hash);
        else{
            names.put(n.hash, names.size());
            nodename = "n"+names.get(n.hash);
        }
        // export
        return "<node id=\""+nodename+"\">\n" +
                "      <data key=\"d5\"/>\n" +
                "      <data key=\"d6\">\n" +
                "        <y:ShapeNode>\n" +
                "          <y:Geometry height=\"30.0\" width=\"30.0\" x=\"370.0\" y=\"123.0\"/>\n" +
                "          <y:Fill color=\"#CC5500\" transparent=\"false\"/>\n" +
                "          <y:BorderStyle color=\"#000000\" type=\"line\" width=\"1.0\"/>\n" +
                "          <y:NodeLabel alignment=\"center\" autoSizePolicy=\"content\" fontFamily=\"Dialog\" fontSize=\"12\" fontStyle=\"plain\" hasBackgroundColor=\"false\" hasLineColor=\"false\" height=\"17.96875\" modelName=\"custom\" textColor=\"#000000\" visible=\"true\" width=\"11.634765625\" x=\"9.1826171875\" y=\"6.015625\">1<y:LabelModel>\n" +
                "              <y:SmartNodeLabelModel distance=\"4.0\"/>\n" +
                "            </y:LabelModel>\n" +
                "            <y:ModelParameter>\n" +
                "              <y:SmartNodeLabelModelParameter labelRatioX=\"0.0\" labelRatioY=\"0.0\" nodeRatioX=\"0.0\" nodeRatioY=\"0.0\" offsetX=\"0.0\" offsetY=\"0.0\" upX=\"0.0\" upY=\"-1.0\"/>\n" +
                "            </y:ModelParameter>\n" +
                "          </y:NodeLabel>\n" +
                "          <y:Shape type=\"rectangle\"/>\n" +
                "        </y:ShapeNode>\n" +
                "      </data>\n" +
                "    </node>";
        
    }
}
