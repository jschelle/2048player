/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;

import board.Direction;
import board.Metrics;
import board.Motion;
import board.Util;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author joris
 */
public class AlphaBetaTreeNode {
      
    // game info
    public int[][] board;
    public Direction dir;

    // heuristics
    public double score;

    // linkage
    public int depth;  
    public AlphaBetaTreeNode parent = null;
    public List<AlphaBetaTreeNode> children = new ArrayList<>();
    
    // hash
    public String hash;
    private static final String hashChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  
    public AlphaBetaTreeNode(int[][] b){
        board = b;
        calcHash();
        children.clear();
        score = -1;        
    }
    
    private int log2(int i){
        return (int) (java.lang.Math.log(i)/java.lang.Math.log(2));
    }
    private void calcHash(){
        hash = "";
        for(int i=0;i<board.length;i++){
            for(int j=0;j<board.length;j++){                
                hash+=hashChars.charAt(board[i][j]==0?0:log2(board[i][j]));
            }
        }
    }
    
 private List<AlphaBetaTreeNode> expandByMotion(AlphaBetaTreeNode n){
                
        final int[][] b = n.board;
        List<AlphaBetaTreeNode> retval = new ArrayList<>();
        int[][] up = Motion.up(b);      
        int[][] right = Motion.right(b);  
        int[][] down = Motion.down(b);        
        int[][] left = Motion.left(b);
                
        if(!Util.equals(b, up)){
            AlphaBetaTreeNode child = new AlphaBetaTreeNode(up);
            child.dir = Direction.UP;
            retval.add(child);
        }
         
        if(!Util.equals(b, right)){
            AlphaBetaTreeNode child = new AlphaBetaTreeNode(right);
            child.dir = Direction.RIGHT;
            retval.add(child);
        }
         
        if(!Util.equals(b, down)){
            AlphaBetaTreeNode child = new AlphaBetaTreeNode(down);
            child.dir = Direction.DOWN;
            retval.add(child);
        }
         
        if(!Util.equals(b, left)){
            AlphaBetaTreeNode child = new AlphaBetaTreeNode(left);
            child.dir = Direction.LEFT;
            retval.add(child);
        }
              
        return retval;        
    }
    private List<AlphaBetaTreeNode> expandByRandom(AlphaBetaTreeNode n){
        List<AlphaBetaTreeNode> retval = new ArrayList<>();
        // board shortcut
        int[][] b = n.board;
        // find empty spots
        List<Point> empty = new ArrayList<>();
        for(int i=0;i<b.length;i++)
            for(int j=0;j<b.length;j++)
                if(b[i][j]==0)
                    empty.add(new Point(i,j));
        // possible values for empty spot
        int[] possibleForNew = {2,4};
        // iterate over empty spots        
        for(int i=0;i<empty.size();i++){
            // iterate over possible values
            for(int v0 : possibleForNew){
                int[][] cop = Util.deepCopy(b);
                cop[empty.get(i).x][empty.get(i).y] = v0;
                retval.add(new AlphaBetaTreeNode(cop));                
            }            
        }       
        // return
        return retval;
    }
    public void expand(){
        List<AlphaBetaTreeNode> retval = new ArrayList<>();
        // maximizing player
        if((depth%2)==0){
            retval.addAll(expandByMotion(this));
        }
        // minimizing player
        else{
            retval.addAll(expandByRandom(this));
        }
        // set depth
        for(AlphaBetaTreeNode child : retval){
            child.depth = depth+1;
            child.parent = this;
        }
        // set children
        children.addAll(retval);
    }
                    
}
    
