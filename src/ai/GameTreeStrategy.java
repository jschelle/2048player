/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;

import board.Direction;
import pkg2048_player.Strategy;

/**
 *
 * @author joris
 */
public class GameTreeStrategy implements Strategy{

    private AlphaBetaGameTree tree;
    
    @Override
    public Direction decide(int[][] board) {  
        
        // solve
        tree = new AlphaBetaGameTree(board,6);
        
        // return
        return tree.findHint();        
    }
    
    @Override
    public void reset(){
        tree=null;
    }
    
    @Override
    public boolean isDone() {
        if(tree==null)
            return false;
        return tree.isDone();
    }
    
    
}
