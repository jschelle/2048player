/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;

import board.Metrics;

/**
 *
 * @author joris
 */
public class NodeScore {
    
    private static double[] weights = {3,99,3,56,100};
    private static double[] bonus = {0,1000,1000,0,1000};
    
    public static void setWeights(double[] w){
        weights = w;
    }
    public static double[] getWeights(){
        return weights;
    }
    public static void scoreNode(AlphaBetaTreeNode n){
        int[][] brd = n.board;
        double[] c = new double[5];
        c[0] = Metrics.emptySquares(brd);
        c[1] = Metrics.highestValue(brd);
        c[2] = Metrics.bottomHeavy(brd);  
        c[3] = Metrics.bottomMonotone(brd);
        c[4] = Metrics.gameOverMetric(brd);         
        
        double score = 0.0;
        for(int i=0;i<c.length;i++){
            score+=weights[i]*c[i];
            if(c[i]==1.0)
                score+=bonus[i];
        }
        
        n.score = score;
    }
}
