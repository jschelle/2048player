/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;

import board.Direction;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author joris
 */
public class AlphaBetaGameTree {
        
    private final int max_depth;
    private final AlphaBetaTreeNode m_root;
    private boolean m_done;
    
    public AlphaBetaGameTree(int[][] b, int d){
        m_root = makeNode(b);
        max_depth = d;
        m_root.depth = 0;
        m_done = false;
    }
    
    public boolean isDone(){
        return m_done;
    }
    public Direction findHint(){                  
        try{
            AlphaBetaTreeNode n = findBestLeaf();
            while(!n.parent.equals(m_root)){
                n = n.parent;
            }
            return n.dir;              
        }catch(NullPointerException ex){
            m_done = true;
            return Direction.DOWN;
        }      
    }
    private AlphaBetaTreeNode findBestLeaf(){
        double best = alphabeta(m_root,-Double.MAX_VALUE,Double.MAX_VALUE);
        for(AlphaBetaTreeNode leaf : leaves()){
            if(isSolution(leaf,best)){
                return leaf;
            }
        }
        return null;
    }
    private boolean isSolution(AlphaBetaTreeNode leaf, double v){
//        if(leaf.score!=v)
//            return false;       
        while(leaf.parent!=null && leaf.parent.score==leaf.score){
            leaf = leaf.parent;
        }
        if(leaf.parent==null)
            return true;
        return false;
    }
    private List<AlphaBetaTreeNode> leaves(){
        List<AlphaBetaTreeNode> retval = new ArrayList<>();
        Stack<AlphaBetaTreeNode> candidates = new Stack<>();
        candidates.add(m_root);
        while(!candidates.empty()){
            AlphaBetaTreeNode n = candidates.pop();
            if(n.depth==max_depth || n.children.isEmpty())
                retval.add(n);
            candidates.addAll(n.children);
        }
        return retval;
    }

    private void scoreNode(AlphaBetaTreeNode n){
        NodeScore.scoreNode(n);
    }
    private AlphaBetaTreeNode makeNode(int[][] b){
        AlphaBetaTreeNode n = new AlphaBetaTreeNode(b);
        return n;
    }
       
    private double alphabeta(AlphaBetaTreeNode n, double alpha, double beta){  
        if(n.depth==max_depth){
            // return the heuristic value of node
            scoreNode(n);
            return n.score;
        }
        // maximizing player
        if((n.depth%2)==0){
            // expand children
            if(n.children.isEmpty())
                n.expand();
            // terminal node
            if(n.children.isEmpty()){
                scoreNode(n);
                return n.score;
            }             
            for(AlphaBetaTreeNode child : n.children){
                alpha = java.lang.Math.max(alpha, alphabeta(child,alpha,beta));
                if(beta<=alpha)
                    break;
            }
            n.score = alpha;
            return alpha;
        }
        // minimizing player
        else{
            // expand children
            if(n.children.isEmpty())
                n.expand();
            // terminal node
            if(n.children.isEmpty()){
                scoreNode(n);
                return n.score;
            }                
            for(AlphaBetaTreeNode child : n.children){
                beta = java.lang.Math.min(beta,alphabeta(child,alpha,beta));
                if(beta<=alpha)
                    break;
            }
            n.score = beta;
            return beta;
        }            
    }
}
